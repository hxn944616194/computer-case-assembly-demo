﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using HighlightingSystem;
public class AssembleTrigger : MonoBehaviour
{
    public GameObject Part;
    public string Tip;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject==Part)
        {
            GetComponent<Highlighter>().FlashingOff();
           // Part.GetComponent<VRTK_InteractableObject>().isGrabbable = false;
          //  Part.GetComponent<VRTK_InteractableObject>().GetGrabbingObject().GetComponent<VRTK_InteractGrab>().ForceRelease();
            Part.transform.rotation = transform.rotation;
            Part.transform.position = transform.position;
            Part.GetComponent<Collider>().enabled = false;
            GetComponent<Collider>().enabled = false;
            GameManager.instance.Next();
        }
    }

    internal void Assemble()
    {
        GetComponent<Collider>().enabled = true;
       // Part.GetComponent<VRTK_InteractableObject>().InteractableObjectGrabbed += AssembleTrigger_InteractableObjectGrabbed;
        Part.GetComponent<AssemblePart>().Assemble();
    }

   // private void AssembleTrigger_InteractableObjectGrabbed(object sender, InteractableObjectEventArgs e)
  //  {
      //  GetComponent<Highlighter>().FlashingOn();
  //  }
}
