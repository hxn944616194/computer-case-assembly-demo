﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HighlightingSystem;

public class AssemblePart : MonoBehaviour
{ 
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    internal void Assemble()
    {

        GetComponent<Highlighter>().FlashingOn();
 
        GetComponent<Collider>().enabled = true;
    }

    //private void AssemblePart_InteractableObjectGrabbed(object sender, InteractableObjectEventArgs e)
    //{
    //    GetComponent<Highlighter>().FlashingOff();
    //}
}
