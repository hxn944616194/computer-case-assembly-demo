﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HighlightingSystem;
//using VRTK;
public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    // Start is called before the first frame update
    public AssembleTrigger[] assembles;
    
    public int Index;
    public GameObject Switch;
    public GameObject Page0;
    public GameObject Page1;
    public GameObject Page2;



    void Start()
    {
        if(instance==null)
        {
            instance = this;
            StartAssemble(0);
        }
    }
    public void Next()
    {
        StartAssemble( Index+1);
    }
    public void StartAssemble(int v)
    {
        Index = v;
        if(assembles.Length>Index)
        {
            assembles[Index].Assemble();
            TipManager.instance.SetText(assembles[Index].Tip);
        }else
        {
            SwichOn(); 
        }
    }

    private void SwichOn()
    {
        TipManager.instance.SetText("开机");
        Switch.GetComponent<Highlighter>().FlashingOn();
       // Switch.GetComponent<VRTK_InteractableObject>().InteractableObjectTouched += GameManager_InteractableObjectTouched;
    }

    //private void GameManager_InteractableObjectTouched(object sender, InteractableObjectEventArgs e)
    //{
    //    TipManager.instance.Hide();
    //    //  Switch.GetComponent<VRTK_InteractableObject>().InteractableObjectTouched -= GameManager_InteractableObjectTouched;
    //    Switch.GetComponent<Highlighter>().FlashingOff();
    //    StartCoroutine(TurnOn());
    //}

    private IEnumerator TurnOn()
    {
        Page0.SetActive(false);
        Page1.SetActive(true);
        yield return new WaitForSeconds(3);
        Page1.SetActive(false);
        Page2.SetActive(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
     
}
