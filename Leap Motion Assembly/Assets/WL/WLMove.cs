﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Keyboard control direction movement
/// </summary>
public class WLMove : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float BallX = Input.GetAxis("Horizontal");
        float BallY = Input.GetAxis("Vertical");
        transform.position += new Vector3(0, BallY, -BallX)*Time.deltaTime;
    }
}
