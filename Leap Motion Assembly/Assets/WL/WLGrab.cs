﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WLGrab : MonoBehaviour
{
    public Vector3 p;
    public Vector3 r;
    // Start is called before the first frame update
    void Start()
    {
        p = transform.position;
        r = transform.eulerAngles;
    }
    /// <summary>
    /// recover
    /// </summary>
    public void Recover()
    {
        transform.position = p;
        transform.eulerAngles = r;
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.tag=="shou")//touch by hand
        {
            if (WLGameManager.wLGameManager.recordGame==null&& WLGameManager.wLGameManager.isMake)// if condition that hand didn't touch components and make fist
            {
                WLGameManager.wLGameManager.StartGrad(gameObject);
            }
        }

    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "t")
        {
            WLGameManager.wLGameManager.AnZhuang();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
