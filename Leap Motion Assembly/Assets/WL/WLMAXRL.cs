﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Left Right rotation
/// </summary>
public class WLMAXRL : MonoBehaviour
{
    /// <summary>
    /// Assembly
    /// </summary>
    public bool isAssembly=true;

    public GameObject assembly;
    public GameObject separate;
    bool isPlay=false;
    void Start()
    {
        
    }
    public void L()
    {
        if (isPlay)
        {
            isAssembly = true;
        }
        
    }
   public void R()
    {
        if (isPlay)
        {
            isAssembly = false;
        }
        
    }
    public void Play(bool data)
    {
        isPlay = data;
    }


    public void ADD()
    {
        if (isAssembly)
        {
            SceneManager.LoadScene("Assembly");
        }
        else
        {
            SceneManager.LoadScene("Separate");
        }
        
    }
    void Update()
    {
        assembly.SetActive(!isAssembly);
        separate.SetActive(isAssembly);
    }
}
