﻿using System.Collections;
using System.Collections.Generic;
using HighlightingSystem;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
/// <summary>
/// Game Manager
/// </summary>
public class WLGameManager : MonoBehaviour
{
    public static WLGameManager wLGameManager;

    public GameObject[] models; // For the all computer components models

    public GameObject[] targets;// For the position of targets in scane

    public GameObject endGame;  // The animation of end of game

    /// <summary>
    /// Define text when finish assembly
    /// </summary>
    public Text text;
    public string dataText = "Finish";

    private void Awake()
    {
        wLGameManager = this;
    }
    void Start()
    {
        StartGame();
    }

    /// <summary>
    /// Initialization highlight condition
    /// </summary>
    public void Initialize()
    {
        // Set condition to turn off highlight when assembly 
        foreach (var item in models)
        {
            item.GetComponentInChildren<Highlighter>().FlashingOff();
        }
        foreach (var item in targets)
        {
            item.GetComponentInChildren<Highlighter>().FlashingOff();
        }
    }
    int plan = 0;
    /// <summary>
    /// Start game method
    /// </summary>
    public void StartGame()
    {
        plan = 0;  // set variable "plan" to counter number of components already assembly
        Next();
    }
    public void ADD()
    {
        if (isAdd)
        {
            SceneManager.LoadScene("MAX");
        }

    }
    bool isAdd = false;

    /// <summary>
    /// Next method
    /// </summary>
    public void Next()
    {
        // Write loop judge whether all components are assemblied and display the text 
        if (plan>= models.Length)
        {
            plan++;
            print("Assembly finish");
            Initialize();

            text.text = dataText;
            isAdd = true;
            return;
        }

        //ShowHint
        foreach (var item in models)
        {
            item.GetComponentInChildren<Highlighter>().FlashingOff();
        }
        models[plan].GetComponentInChildren<Highlighter>().FlashingOn();

        foreach (var item in targets)
        {
            item.GetComponentInChildren<Highlighter>().FlashingOff();
        }
        targets[plan].GetComponentInChildren<Highlighter>().FlashingOn();

        //Activator triggers
        foreach (var item in models)
        {
            item.GetComponentInChildren<Collider>().enabled=false;
        }
        models[plan].GetComponentInChildren<Collider>().enabled = true;
        foreach (var item in targets)
        {
            item.GetComponentInChildren<Collider>().enabled = false;
        }
        targets[plan].GetComponentInChildren<Collider>().enabled = true;
        plan++;
    }
    /// <summary>
    /// Public variable to record components which be catched 
    /// </summary>
    public GameObject recordGame;
    /// <summary>
    /// Judge whether make a fist
    /// </summary>
    public bool isMake = false;
    /// <summary>
    /// Right hand
    /// </summary>
    public GameObject r_hand;
    public void MakeYes()
    {
        isMake = true;
    }
    public void MakeNo()
    {
        isMake = false;
        EndGrad();
    }

    /// <summary>
    /// Begin catch
    /// </summary>
    public void StartGrad(GameObject game)
    {
        recordGame = game;
        recordGame.transform.parent = r_hand.transform;
    }
    /// <summary>
    /// End catch
    /// </summary>
    public void EndGrad()
    {
        if (recordGame)
        {
            recordGame.transform.parent = null;
            recordGame.GetComponent<WLGrab>().Recover();
            recordGame = null;
        }
      
    }
    /// <summary>
    /// Assembly
    /// </summary>
    public void AnZhuang()
    {
        if (recordGame)
        {
            
            recordGame.transform.parent = null;
            recordGame = null;
            models[plan-1].transform.position = targets[plan-1].transform.position;
            models[plan-1].transform.eulerAngles = targets[plan-1].transform.eulerAngles;
            Initialize();
            Next();
        }

    }
    // Update is called once per frame
    void Update()
    {
        // judge whether assembly complete
        if (plan> models.Length)
        {
            for (int i = 0; i < models.Length; i++)
            {
                if (i < plan - 1)
                {
                    models[i].transform.position = targets[i].transform.position;
                    models[i].transform.eulerAngles = targets[i].transform.eulerAngles;
                }
            }
            Initialize();
            return;
        }

        // refresh hint
        if (plan-1 < models.Length&& plan>=0)
        {
            models[plan - 1].GetComponentInChildren<Highlighter>().FlashingOn();
            targets[plan - 1].GetComponentInChildren<Highlighter>().FlashingOn();
            for (int i = 0; i < models.Length; i++)
            {
                if (i< plan - 1)
                {
                    models[i].transform.position = targets[i].transform.position;
                    models[i].transform.eulerAngles = targets[i].transform.eulerAngles;
                }
            }
        }
       
    }
}
